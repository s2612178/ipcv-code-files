clear;close all; clc;
%% Question 1
data = im2double(imread("car_dis.png"));
[N,M] = size(data);
figure(Name="Orginal Image");
imshow(data);
% Calculate and display the fourier transform
f_data = fft2(data);
plot_spectrum(f_data,"Fourier transform","Q1, Fourier transform");
% Save figure
print -r150 -dpng Imlogmag.png

%% Question 3
h = fspecial("average",[1,9]);
filtered = imfilter(data,h,"replicate","corr");
figure(Name="Q3, Using fspecial and imfilter")
imshow(filtered,[])
print -r150 -dpng Q3_filter_image.png
% Calculate and display the fourier transform
f_filtered = fft2(filtered);
plot_spectrum(f_filtered,"Fourier transform","Q3, Fourier transform");
% Save figure
print -r150 -dpng Q3_filter_Fourier.png

%% Question 4
OTF = psf2otf(h,[N,M]);
figure(Name="Q4, OTF");
imshow(fftshift(abs(OTF)),[])
label_figure("u","v","abs(OTF)",true);
print -r150 -dpng Q4_abs_OTF.png
fprintf("Q4.b Attenuation factor: %f\n",OTF(129,157));

%% Question 5
sx = 25;  % Horizontal size (should be uneven)
sy = 13;  % Vertical size  (should be uneven)
row_f = 129; col_f = 157;
r0 = row_f-(sy-1)/2;   r1 = row_f+(sy-1)/2;
c0 = col_f-(sx-1)/2;   c1 = col_f+(sx-1)/2;
H = ones(N,M);
H([r0:r1 r0:r1],[M-c1+2:M-c0+2 c0:c1]) = 0;
H = circshift(H,-floor(size(H)/2));
f_data_filtered = f_data .* H;
figure(Name="Q5 OTF"); imshow(fftshift(abs(H)),[]); label_figure("u","v","abs(OTF)",true);
print -r150 -dpng Q5_OTF.png
% Transform back
inv = ifft2(f_data_filtered);
fprintf("Q5 max imag component: %i\n",max(abs(imag(inv(:)))))
figure(Name=sprintf("Q5, %i, %i",sx,sy)); imshow(inv,[])
gca.Visible = 'off';
print -r150 -dpng Q5_filtered_image.png
plot_spectrum(fft2(inv),"Fourier transform newly filtered image","Q5, Fourier transform");
print -r150 -dpng Q5_OTF_FFT.png
%% Functions
function plot_spectrum(data,title_str,fig_title)
    figure(Name=fig_title);
    imagesc(log(fftshift(abs(data))+1));
    colormap("turbo"); colorbar();
    label_figure("u","v",title_str,true);
end

function label_figure(x_label,y_label,title_str,hide_axis)
    if (hide_axis)
        ax = gca;
        ax.XTick = [];
        ax.YTick = [];
    end
    title(title_str);
    xlabel(x_label);
    ylabel(y_label);
end